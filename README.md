# Sensor para Tragantes

El Sensor para Tragantes es un dispositivo electrónico que
monitorea el nivel de llenado de un basurero y reporta
cuando este está lleno a través de señales de radio.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Sensor para Tragantes" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: w2k31984

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Transmisor de Radio             |        1 |
| Sensor Ultrasónico              |        1 |
| Porta-baterías                  |        2 |
| Cable Jumper                    |        9 |
| Breadboard                      |        1 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino. Las que comienzan con **PB** se conectan al
porta-baterías. Las que comienzan con **RA** se conectan
al radio-transmisor.

| Componente                         |        |         |        |        |
|------------------------------------|--------|---------|--------|--------|
| Ultrasónico                        | BB-J57 | BB-J58  | BB-J59 | BB-J60 |
| Portabatería 1                     | BB-B25 | BB-V+   |        |        |
| Portabatería 2                     | BB-V-  | BB-A25  |        |        |
| Jumper 1                           | BB-V-  | AR-GND  |        |        |
| Jumper 2                           | BB-V+  | AR-VIN  |        |        |
| Jumper 3                           | BB-F57 | AR-5V   |        |        |
| Jumper 4                           | BB-F58 | AR-3    |        |        |
| Jumper 5                           | BB-F59 | BB-G19  |        |        |
| Jumper 6                           | BB-F60 | BB-V-   |
| Jumper 7                           | AR-5   | RA-DATA |
| Jumper 8                           | BB-V+  | RA-VCC  |
| Jumper 9                           | BB-V-  | RA-GND  |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_b2***.

