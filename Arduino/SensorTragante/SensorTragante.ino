#include <HCSR04.h>
#include <NeoSWSerial.h>

NeoSWSerial nss( 4, 5 );
HCSR04 hc(3,2);

void setup() {
  nss.begin();
}

void loop() {
  // Transmite cada vez que se llega a la distancia de llenado
  if( hc.dist() < 200) {
    nss.write('f');
  }
}
